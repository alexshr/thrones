package com.softdesign.gameofthrones.data.managers;

import com.softdesign.gameofthrones.data.network.RestService;
import com.softdesign.gameofthrones.data.network.ServiceGenerator;
import com.softdesign.gameofthrones.data.network.res.CharacterRes;
import com.softdesign.gameofthrones.data.network.res.HouseRes;
import com.softdesign.gameofthrones.data.storage.models.DaoSession;
import com.softdesign.gameofthrones.utils.MyApplication;

import java.util.List;

import retrofit2.Call;

public class DataManager {


    private static DataManager INSTANCE;
    //private PreferencesManager mPreferencesManager;
    private RestService mRestService;//

    private DaoSession mDaoSession;


    private DataManager() {

        //mPreferencesManager = new PreferencesManager();
        mRestService = ServiceGenerator.createService(RestService.class);

        mDaoSession = MyApplication.getDaoSession();


    }

    public static DataManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }


    /**
     * rest api house requset
     *
     * @param id
     * @return
     */
    public Call<HouseRes> getHouseById(long id) {
        return mRestService.getHouseById(id);
    }

    /**
     * rest api character requset
     *
     * @param id
     * @return
     */
    public Call<CharacterRes> getCharacterById(Long id) {
        return mRestService.getCharacterById(id);
    }

    public Call<List<CharacterRes>> getCharacterList(int page) {
        return mRestService.getCharacterList(page);
    }


    public DaoSession getDaoSession() {
        return mDaoSession;
    }


}