package com.softdesign.gameofthrones.data.storage.models;

import com.softdesign.gameofthrones.data.network.res.CharacterRes;
import com.softdesign.gameofthrones.utils.Utils;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;

import java.util.ArrayList;
import java.util.List;

@Entity(active = true, nameInDb = "characters")
public class Character {


    private String characterName;
    private Long fatherRemoteId;
    private Long motherRemoteId;

    @Id
    private Long id;

    //не уникальный, т.к. создаю отдельный объект для каждого дома из за неверной модели
    @NotNull
    private Long remoteId;

    //некоректная модель - может быть неск. домов(((
    private Long houseRemoteId;

    private String born;
    private String died;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "characterRemoteId")
    })
    private List<Title> titleList;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "characterRemoteId")
    })
    private List<Alias> aliasList;


/*
//извращение из-за того что в @ToOne нет referencedName
    @ToMany(joinProperties = {
            @JoinProperty(name = "fatherRemoteId", referencedName = "remoteId")
    })
    private List<Character> fatherList;

    //извращение из-за того что в @ToOne нет referencedName
    @ToMany(joinProperties = {
            @JoinProperty(name = "motherRemoteId", referencedName = "remoteId")
    })
    private List<Character> motherList;
    */
    /**
     * Used for active entity operations.
     */

    @Generated(hash = 898307126)
    private transient CharacterDao myDao;
    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;


    public Character(House house, CharacterRes characterRes) {
        houseRemoteId = house.getRemoteId();
        remoteId = Utils.getIdFromUrl(characterRes.getUrl());
        born = characterRes.getBorn();
        died = characterRes.getDied();
        fatherRemoteId = Utils.getIdFromUrl(characterRes.getFather());
        motherRemoteId = Utils.getIdFromUrl(characterRes.getMother());
        characterName = characterRes.getName();
        titleList = new ArrayList<>();//to avoid db req in getTitleList();
        aliasList = new ArrayList<>();

    }


    @Generated(hash = 1448337972)
    public Character(String characterName, Long fatherRemoteId,
                     Long motherRemoteId, Long id, @NotNull Long remoteId,
                     Long houseRemoteId, String born, String died) {
        this.characterName = characterName;
        this.fatherRemoteId = fatherRemoteId;
        this.motherRemoteId = motherRemoteId;
        this.id = id;
        this.remoteId = remoteId;
        this.houseRemoteId = houseRemoteId;
        this.born = born;
        this.died = died;
    }


    @Generated(hash = 1853959157)
    public Character() {
    }


    public void addTitle(Title title) {

        if (titleList == null) {
            titleList = new ArrayList<>();
        }
        titleList.add(title);
    }

    public void addAlias(Alias alias) {
        if (aliasList == null) {
            aliasList = new ArrayList<>();
        }
        aliasList.add(alias);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1986563529)
    public synchronized void resetAliasList() {
        aliasList = null;
    }


    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 190023459)
    public List<Alias> getAliasList() {
        if (aliasList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            AliasDao targetDao = daoSession.getAliasDao();
            List<Alias> aliasListNew = targetDao._queryCharacter_AliasList(remoteId);
            synchronized (this) {
                if (aliasList == null) {
                    aliasList = aliasListNew;
                }
            }
        }
        return aliasList;
    }


    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 2027530363)
    public synchronized void resetTitleList() {
        titleList = null;
    }


    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 13088488)
    public List<Title> getTitleList() {
        if (titleList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            TitleDao targetDao = daoSession.getTitleDao();
            List<Title> titleListNew = targetDao._queryCharacter_TitleList(remoteId);
            synchronized (this) {
                if (titleList == null) {
                    titleList = titleListNew;
                }
            }
        }
        return titleList;
    }


    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }


    public String getDied() {
        return this.died;
    }


    public void setDied(String died) {
        this.died = died;
    }


    public String getBorn() {
        return this.born;
    }


    public void setBorn(String born) {
        this.born = born;
    }


    public Long getHouseRemoteId() {
        return this.houseRemoteId;
    }


    public void setHouseRemoteId(Long houseRemoteId) {
        this.houseRemoteId = houseRemoteId;
    }


    public Long getRemoteId() {
        return this.remoteId;
    }


    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }


    public Long getId() {
        return this.id;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public Long getMotherRemoteId() {
        return this.motherRemoteId;
    }


    public void setMotherRemoteId(Long motherRemoteId) {
        this.motherRemoteId = motherRemoteId;
    }


    public Long getFatherRemoteId() {
        return this.fatherRemoteId;
    }


    public void setFatherRemoteId(Long fatherRemoteId) {
        this.fatherRemoteId = fatherRemoteId;
    }


    public String getCharacterName() {
        return this.characterName;
    }


    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }


}
