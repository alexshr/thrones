package com.softdesign.gameofthrones.data.network.res;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import javax.annotation.Generated;

//@Generated("org.jsonschema2pojo")
public class CharacterRes {

    private String url;
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private List<String> titles = new ArrayList<String>();
    private List<String> aliases = new ArrayList<>();
    private String father;
    private String mother;
    private String spouse;
    private List<String> allegiances = new ArrayList<String>();
    private List<String> books = new ArrayList<String>();
    private List<Object> povBooks = new ArrayList<Object>();
    private List<Object> tvSeries = new ArrayList<Object>();
    private List<Object> playedBy = new ArrayList<Object>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    /**
     * alexshr
     */


    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The culture
     */
    public String getCulture() {
        return culture;
    }

    /**
     * @param culture The culture
     */
    public void setCulture(String culture) {
        this.culture = culture;
    }

    /**
     * @return The born
     */
    public String getBorn() {
        return born;
    }

    /**
     * @param born The born
     */
    public void setBorn(String born) {
        this.born = born;
    }

    /**
     * @return The died
     */
    public String getDied() {
        return died;
    }

    /**
     * @param died The died
     */
    public void setDied(String died) {
        this.died = died;
    }

    /**
     * @return The titles
     */
    public List<String> getTitles() {
        return titles;
    }

    /**
     * @param titles The titles
     */
    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    /**
     * @return The aliases
     */
    public List<String> getAliases() {
        return aliases;
    }

    /**
     * @param aliases The aliases
     */
    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    /**
     * @return The father
     */
    public String getFather() {
        return father;
    }

    /**
     * @param father The father
     */
    public void setFather(String father) {
        this.father = father;
    }

    /**
     * @return The mother
     */
    public String getMother() {
        return mother;
    }

    /**
     * @param mother The mother
     */
    public void setMother(String mother) {
        this.mother = mother;
    }

    /**
     * @return The spouse
     */
    public String getSpouse() {
        return spouse;
    }

    /**
     * @param spouse The spouse
     */
    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    /**
     * @return The allegiances
     */
    public List<String> getAllegiances() {
        return allegiances;
    }

    /**
     * @param allegiances The allegiances
     */
    public void setAllegiances(List<String> allegiances) {
        this.allegiances = allegiances;
    }

    /**
     * @return The books
     */
    public List<String> getBooks() {
        return books;
    }

    /**
     * @param books The books
     */
    public void setBooks(List<String> books) {
        this.books = books;
    }

    /**
     * @return The povBooks
     */
    public List<Object> getPovBooks() {
        return povBooks;
    }

    /**
     * @param povBooks The povBooks
     */
    public void setPovBooks(List<Object> povBooks) {
        this.povBooks = povBooks;
    }

    /**
     * @return The tvSeries
     */
    public List<Object> getTvSeries() {
        return tvSeries;
    }

    /**
     * @param tvSeries The tvSeries
     */
    public void setTvSeries(List<Object> tvSeries) {
        this.tvSeries = tvSeries;
    }

    /**
     * @return The playedBy
     */
    public List<Object> getPlayedBy() {
        return playedBy;
    }

    /**
     * @param playedBy The playedBy
     */
    public void setPlayedBy(List<Object> playedBy) {
        this.playedBy = playedBy;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}