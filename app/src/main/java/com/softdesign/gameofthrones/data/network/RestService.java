package com.softdesign.gameofthrones.data.network;

import com.softdesign.gameofthrones.data.network.res.CharacterRes;
import com.softdesign.gameofthrones.data.network.res.HouseRes;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * REST сервис (интерфейс для описания запросов к REST API)
 */
public interface RestService {

    @GET("houses/{id}")
    Call<HouseRes> getHouseById(@Path("id") long houseId);


    @GET("characters/{id}")
    Call<CharacterRes> getCharacterById(@Path("id") Long characterId);

    @GET("characters?pageSize=50")
    Call<List<CharacterRes>> getCharacterList(@Query("page") int page);


}
