package com.softdesign.gameofthrones.data.storage.models;

import com.softdesign.gameofthrones.data.network.res.HouseRes;
import com.softdesign.gameofthrones.utils.Utils;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.ArrayList;
import java.util.List;

@Entity(active = true, nameInDb = "houses")
public class House {

    @Id
    private Long id;

    //private String words;

    private int characterCount;

    @NotNull
    @Unique
    private Long remoteId;

    /*
    @NotNull
    @Unique
    private String name;
*/
    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "houseRemoteId")
    })
    private List<Character> characterList;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1167916919)
    private transient HouseDao myDao;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;


    public House(HouseRes houseRes, String name) {
        remoteId = Utils.getIdFromUrl(houseRes.getUrl());
        //this.name = name;
        //words = houseRes.getWords();
        characterCount = houseRes.getSwornMembers().size();
        characterList = new ArrayList<>();// to avoid db req call

    }


    @Generated(hash = 1522470533)
    public House(Long id, int characterCount, @NotNull Long remoteId) {
        this.id = id;
        this.characterCount = characterCount;
        this.remoteId = remoteId;
    }


    @Generated(hash = 389023854)
    public House() {
    }


    public void addCharacter(Character character) {
        if (characterList == null) {
            characterList = new ArrayList<>();
        }
        characterList.add(character);
    }

    public boolean isAllCharacters() {
        return getCharacterList() != null && getCharacterList().size() == characterCount;
    }

    public void addCharacterList(Character character) {
        characterList.add(character);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }


    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }


    /**
     * Resets a to-many relationship, making the next get call to query for a fresh result.
     */
    @Generated(hash = 1221750398)
    public synchronized void resetCharacterList() {
        characterList = null;
    }


    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1617314533)
    public List<Character> getCharacterList() {
        if (characterList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterDao targetDao = daoSession.getCharacterDao();
            List<Character> characterListNew = targetDao._queryHouse_CharacterList(remoteId);
            synchronized (this) {
                if (characterList == null) {
                    characterList = characterListNew;
                }
            }
        }
        return characterList;
    }


    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 451323429)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getHouseDao() : null;
    }


    public Long getRemoteId() {
        return this.remoteId;
    }


    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }


    public int getCharacterCount() {
        return this.characterCount;
    }


    public void setCharacterCount(int characterCount) {
        this.characterCount = characterCount;
    }


    public Long getId() {
        return this.id;
    }


    public void setId(Long id) {
        this.id = id;
    }


}
