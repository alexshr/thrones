package com.softdesign.gameofthrones.data.storage.models;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

@Entity(active = true, nameInDb = "aliases")
public class Alias {

    @Id
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Long characterRemoteId;

    /**
     * Used for active entity operations.
     */
    @Generated(hash = 1571512818)
    private transient AliasDao myDao;

    /**
     * Used to resolve relations
     */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;


    public Alias(String name, Long characterId) {
        this.name = name;
        this.characterRemoteId = characterId;
    }


    @Generated(hash = 381930840)
    public Alias(Long id, @NotNull String name, @NotNull Long characterRemoteId) {
        this.id = id;
        this.name = name;
        this.characterRemoteId = characterRemoteId;
    }


    @Generated(hash = 1265971347)
    public Alias() {
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * called by internal mechanisms, do not call yourself.
     */
    @Generated(hash = 360304207)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getAliasDao() : null;
    }

    public Long getCharacterRemoteId() {
        return this.characterRemoteId;
    }

    public void setCharacterRemoteId(Long characterRemoteId) {
        this.characterRemoteId = characterRemoteId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
