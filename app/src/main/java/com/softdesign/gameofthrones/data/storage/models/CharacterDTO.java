package com.softdesign.gameofthrones.data.storage.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.softdesign.gameofthrones.data.network.res.CharacterRes;
import com.softdesign.gameofthrones.utils.Utils;

import org.greenrobot.greendao.annotation.NotNull;

import java.util.ArrayList;
import java.util.List;


public class CharacterDTO implements Parcelable {


    public static final Parcelable.Creator<CharacterDTO> CREATOR = new Parcelable.Creator<CharacterDTO>() {
        @Override
        public CharacterDTO createFromParcel(Parcel source) {
            return new CharacterDTO(source);
        }

        @Override
        public CharacterDTO[] newArray(int size) {
            return new CharacterDTO[size];
        }
    };
    private String characterName;
    private Long fatherRemoteId;
    private Long motherRemoteId;
    private String fatherName;
    private String motherName;
    private Long remoteId;
    private Long houseRemoteId;
    private String born;

    //private String houseWords;
    private String died;
    private List<String> titleList = new ArrayList<>();
    private List<String> aliasList = new ArrayList<>();


    public CharacterDTO(Long houseId) {

        this.remoteId = remoteId;


    }


    public CharacterDTO(String characterName,
                        Long father, Long mother,
                        @NotNull Long remoteId,
                        String born, String died) {

        this.houseRemoteId = houseRemoteId;
        this.characterName = characterName;
        this.fatherRemoteId = father;
        this.motherRemoteId = mother;
        this.remoteId = remoteId;
        this.born = born;
        this.died = died;
        //this.titleList = titleList;
        //this.aliasList = aliasList;

    }


    protected CharacterDTO(Parcel in) {
        this.characterName = in.readString();
        this.fatherRemoteId = (Long) in.readValue(Long.class.getClassLoader());
        this.motherRemoteId = (Long) in.readValue(Long.class.getClassLoader());
        this.fatherName = in.readString();
        this.motherName = in.readString();
        this.remoteId = (Long) in.readValue(Long.class.getClassLoader());
        this.born = in.readString();
        this.died = in.readString();
        this.titleList = in.createStringArrayList();
        this.aliasList = in.createStringArrayList();
    }


    /*
    public static CharacterDTO getInstance(CharacterRes character) {
        return new CharacterDTO(character.getName(),
                character.getFatherRemoteId(), character.getMotherRemoteId(),
                character.getRemoteId(), character.getHouseRemoteId(),
                character.getBorn(), character.getDied(),
                character.getTitleList(), character.getAliasList(),
                house.getWords());

    }
    */


/*
    public int getHouseIcon() {
        if (houseRemoteId == HOUSE_STARK_ID) {
            return R.drawable.stark_icon;
        } else if (houseRemoteId == HOUSE_LANNISTER_ID) {
            return R.drawable.lanister_icon;
        } else {
            return R.drawable.targarien_icon;
        }
    }
*/

    public static CharacterDTO getInstance(Character character) {

        CharacterDTO characterDTO = new CharacterDTO(character.getCharacterName(),
                character.getFatherRemoteId(), character.getMotherRemoteId(),
                character.getRemoteId(),
                character.getBorn(), character.getDied());

        List<Title> titles = character.getTitleList();
        List<Alias> aliases = character.getAliasList();

        if (titles != null) {
            for (Title title : titles) {
                characterDTO.addTitle(title.getName());
            }
        }
        if (aliases != null) {
            for (Alias alias : aliases) {
                characterDTO.addAlias(alias.getName());
            }
        }

        return characterDTO;
    }

    public static CharacterDTO getInstance(CharacterRes characterRes) {
        CharacterDTO characterDTO = new CharacterDTO(characterRes.getName(),
                Utils.getIdFromUrl(characterRes.getFather()), Utils.getIdFromUrl(characterRes.getMother()),
                Utils.getIdFromUrl(characterRes.getUrl()),
                characterRes.getBorn(), characterRes.getDied());

        characterDTO.setTitleList(characterRes.getTitles());
        characterDTO.setAliasList(characterRes.getAliases());

        return characterDTO;

    }

    public String getInfoForList(char delimiter) {
        StringBuilder sb = new StringBuilder("");
        if (titleList.size() > 0) {
            for (String title : titleList) {
                sb.append(delimiter).append(title);
            }
            sb.delete(0, 2);
        } else if (aliasList.size() > 0) {
            for (String alias : aliasList) {
                sb.append(delimiter).append(alias);
            }
            sb.delete(0, 2);
        }
        return sb + "";
    }

    public String getInfoForList() {

        String title = Utils.convertList(titleList, ", ");
        if (title == null) {
            return Utils.convertList(aliasList, ", ");
        } else {
            return title;
        }
    }

    public String getFullTitle() {
        return Utils.convertList(titleList, "\n");
    }

    public String getFullAlias() {
        return Utils.convertList(aliasList, "\n");
    }

    public void addTitle(String title) {
        titleList.add(title);
    }

    public void addAlias(String alias) {
        aliasList.add(alias);
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public Long getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public Long getMotherRemoteId() {
        return this.motherRemoteId;
    }

    public void setMotherRemoteId(Long motherRemoteId) {
        this.motherRemoteId = motherRemoteId;
    }

    public Long getFatherRemoteId() {
        return this.fatherRemoteId;
    }

    public void setFatherRemoteId(Long fatherRemoteId) {
        this.fatherRemoteId = fatherRemoteId;
    }

    public String getCharacterName() {
        return this.characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public void setTitleList(List<String> titleList) {
        this.titleList = titleList;
    }

    public void setAliasList(List<String> aliasList) {
        this.aliasList = aliasList;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.characterName);
        dest.writeValue(this.fatherRemoteId);
        dest.writeValue(this.motherRemoteId);
        dest.writeString(this.fatherName);
        dest.writeString(this.motherName);
        dest.writeValue(this.remoteId);
        dest.writeString(this.born);
        dest.writeString(this.died);
        dest.writeStringList(this.titleList);
        dest.writeStringList(this.aliasList);
    }
}
