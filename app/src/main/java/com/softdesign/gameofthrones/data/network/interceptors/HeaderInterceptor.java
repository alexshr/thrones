package com.softdesign.gameofthrones.data.network.interceptors;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Add user auth headers
 */
public class HeaderInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder()
                .header("User-Agent", "GameOfThronesApp")
                .header("Cache-Control", "max-age=" + (60 * 60 * 24));


        /*
        String lastModified = pm.getLastModified();
        if (!lastModified.isEmpty()) {
            requestBuilder.header("If-Modified-Since", pm.getLastModified());
        }
        */

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }

}
