package com.softdesign.gameofthrones.mvp.views;

import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;

import java.util.List;

/**
 * Created by alexshr.
 */

public interface ICharacterListView {

    void showCharacterList(List<CharacterDTO> characterList);


    void startCharacterActivity(CharacterDTO character, int houseIndex);
}
