package com.softdesign.gameofthrones.mvp.presenters;

import android.content.Context;
import android.util.Log;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.mvp.models.AppModel;
import com.softdesign.gameofthrones.mvp.models.IAppModel;
import com.softdesign.gameofthrones.mvp.views.IHousesView;
import com.softdesign.gameofthrones.utils.CharacterEvent;
import com.softdesign.gameofthrones.utils.HouseEvent;
import com.softdesign.gameofthrones.utils.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_ERROR;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_NOT_AVAILABLE;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SERVER_ERROR;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SHOW_PROGRESS;



public class HousesPresenter {

    static HousesPresenter instance=new HousesPresenter();

    IHousesView mHousesView;
    IAppModel mAppModel=new AppModel();


    public static HousesPresenter getInstance(){
        return  instance;
    }

    //region data from model
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event.mes);

        if (event.mes.equals(MES_SHOW_PROGRESS)) {
            mHousesView.showProgress();
        } else if (event.mes.equals(MES_NETWORK_ERROR) || event.mes.equals(MES_SERVER_ERROR)) {
            mHousesView.hideProgress();
            mHousesView.hideSplash();
            mHousesView.showError(R.string.error_server);

        } else if (event.mes.equals(MES_NETWORK_NOT_AVAILABLE)) {
            mHousesView.hideProgress();
            mHousesView.hideSplash();
            mHousesView.showError(R.string.error_no_network);

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CharacterEvent event) {
        mHousesView.hideProgress();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(HouseEvent event) {
        mHousesView.hideSplash();
    }

//endregion

    public void requestData(Context context) {
        mAppModel.requestHouses(context);
    }

    public void takeView(IHousesView view){
        mHousesView=view;
    }

    public void register(){
        EventBus.getDefault().register(this);
    }

    public void unregister(){
        EventBus.getDefault().unregister(this);
    }



}
