package com.softdesign.gameofthrones.mvp.views;

import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;

/**
 * Created by alexshr.
 */

public interface ICharacterView {


    void hideProgress();

    void showError(int mes);

    void showProgress();

    void showCharacter(CharacterDTO character, int houseIndex);

    /**
     * запуск активити с детализацией героя
     * @param character
     * @param houseIndex - индех tab во view pager
     */
    void startCharacterActivity(CharacterDTO character, int houseIndex);
}


