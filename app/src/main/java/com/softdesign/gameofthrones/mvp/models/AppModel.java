package com.softdesign.gameofthrones.mvp.models;

import android.content.Context;

import com.softdesign.gameofthrones.services.DownloadDataService;

/**
 * Created by alexshr.
 */

public class AppModel implements IAppModel {

    /**
     * запрашиваем данные для отображения 3-х домов со списком героев
     * @param context
     */
    @Override
    public void requestHouses(Context context) {
        DownloadDataService.startDownload(context);
    }


    /**
     * запрашиваем данные Character, на который нажали в списке
     * @param context
     * @param characterId
     * @param houseId
     */
    @Override
    public boolean requestCharacter(Context context,Long characterId,int houseId) {
        return DownloadDataService.startDownloadCharacter(context, characterId,houseId);
    }

    /**
     * запрашиваем детализацию father или mother при нажатии на них
     * @param context
     * @param characterId
     * @param parentCharacterId
     * @return запустился ли сервис
     */
    @Override
    public boolean requestParentCharacter(Context context,Long characterId,Long parentCharacterId) {
        return DownloadDataService.startDownloadParentCharacter(context,characterId,parentCharacterId);
    }


}
