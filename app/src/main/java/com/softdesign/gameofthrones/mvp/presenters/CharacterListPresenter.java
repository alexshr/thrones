package com.softdesign.gameofthrones.mvp.presenters;

import android.content.Context;
import android.util.Log;

import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.mvp.models.AppModel;
import com.softdesign.gameofthrones.mvp.models.IAppModel;
import com.softdesign.gameofthrones.mvp.views.ICharacterListView;
import com.softdesign.gameofthrones.utils.CharacterEvent;
import com.softdesign.gameofthrones.utils.HouseEvent;
import com.softdesign.gameofthrones.utils.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SHOW_PROGRESS;

/**
 * Created by alexshr.
 */

public class CharacterListPresenter {

    int mHouseIndex;

    ICharacterListView mView;
    IAppModel mModel=new AppModel();

    public CharacterListPresenter(ICharacterListView view, int houseIndex) {
        mView = view;
        mHouseIndex = houseIndex;
    }

    //region data from model

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(HouseEvent event) {
        Log.d(LOG_TAG, "fragment onMessageEvent " + event);
        if (event.houseIndex == mHouseIndex) {
            mView.showCharacterList(event.characterList);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CharacterEvent event) {
        Log.d(LOG_TAG, "fragment onMessageEvent " + event);
        if (event.childCharacterId == null && event.houseIndex == mHouseIndex) {
                        mView.startCharacterActivity(event.character,mHouseIndex);
        }
    }

//endregion


    public void register() {
        EventBus.getDefault().register(this);
    }

    public void unregister() {
        EventBus.getDefault().unregister(this);
    }

    public void showCharacterList(List<CharacterDTO> characterList) {
        mView.showCharacterList(characterList);
    }

    public void requestCharacter(Context context, Long characterId) {
        if (mModel.requestCharacter(context,characterId,mHouseIndex)) {
            EventBus.getDefault().post(new MessageEvent(MES_SHOW_PROGRESS));
        }
    }

    public int getHouseIndex() {
        return mHouseIndex;
    }
}
