package com.softdesign.gameofthrones.mvp.models;

import android.content.Context;

/**
 * Created by alexshr.
 */

public interface IAppModel {

    void requestHouses(Context context);


    public boolean requestCharacter(Context context,Long characterId,int houseId);

    /**
     * запрос на получение детализации родителя
     * @param context
     * @param characterId  - id текущего character
     * @param parentCharacterId - id одного из родителей (которого мы запросили)
     * @return
     */
    public boolean requestParentCharacter(Context context,Long characterId,Long parentCharacterId);

}
