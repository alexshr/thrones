package com.softdesign.gameofthrones.mvp.presenters;

import android.content.Context;
import android.util.Log;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.mvp.models.AppModel;
import com.softdesign.gameofthrones.mvp.models.IAppModel;
import com.softdesign.gameofthrones.mvp.views.ICharacterView;
import com.softdesign.gameofthrones.utils.CharacterEvent;
import com.softdesign.gameofthrones.utils.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_ERROR;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_NOT_AVAILABLE;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SERVER_ERROR;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SHOW_PROGRESS;


public class CharacterPresenter {

    ICharacterView mView;

    CharacterDTO mCharacter;
    int mHouseIndex;

    IAppModel mAppModel=new AppModel();

    public CharacterPresenter(ICharacterView view) {
               mView=view;
    }

    public void init(CharacterDTO character,int houseIndex){
        mCharacter=character;
        mHouseIndex=houseIndex;
        mView.showCharacter(character,houseIndex);
    }

    //region data from model
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CharacterEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event);
        if (event.childCharacterId.longValue() == mCharacter.getRemoteId().longValue()) {
            //start(this, houseIndex, event.character);
            mView.startCharacterActivity(event.character,mHouseIndex);
            mView.hideProgress();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event.mes);

        if (event.mes.equals(MES_SHOW_PROGRESS)) {
            mView.showProgress();
        } else if (event.mes.equals(MES_NETWORK_ERROR)||event.mes.equals(MES_SERVER_ERROR)) {
            mView.hideProgress();
            mView.showError(R.string.error_server);

        }  else if (event.mes.equals(MES_NETWORK_NOT_AVAILABLE)) {
            mView.hideProgress();
            mView.showError(R.string.error_no_network);

        }

    }
//endregion



    //region eventbus registration
    public void register(){
        EventBus.getDefault().register(this);
    }

    public void unregister(){
        EventBus.getDefault().unregister(this);
    }


    //endregion


    private void requestParentCharacter(Context context,Long parentCharacterId) {

        if (mAppModel.requestParentCharacter(context,mCharacter.getRemoteId(),parentCharacterId)) {
            EventBus.getDefault().post(new MessageEvent(MES_SHOW_PROGRESS));
        }
    }

    public void requestFather(Context context){
        requestParentCharacter(context,mCharacter.getFatherRemoteId());
    }

    public void requestMother(Context context){
        requestParentCharacter(context,mCharacter.getMotherRemoteId());
    }

    public CharacterDTO getCharacter() {
        return mCharacter;
    }
}
