package com.softdesign.gameofthrones.mvp.views;

/**
 * Created by alexshr.
 */

public interface IHousesView {
    void showProgress();

    void hideProgress();

    void showSplash();

    void hideSplash();

    void showError(int mes);


}
