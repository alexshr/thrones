package com.softdesign.gameofthrones.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.softdesign.gameofthrones.data.managers.DataManager;
import com.softdesign.gameofthrones.data.network.res.CharacterRes;
import com.softdesign.gameofthrones.data.network.res.HouseRes;
import com.softdesign.gameofthrones.data.storage.models.Alias;
import com.softdesign.gameofthrones.data.storage.models.AliasDao;
import com.softdesign.gameofthrones.data.storage.models.Character;
import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.data.storage.models.CharacterDao;
import com.softdesign.gameofthrones.data.storage.models.House;
import com.softdesign.gameofthrones.data.storage.models.HouseDao;
import com.softdesign.gameofthrones.data.storage.models.Title;
import com.softdesign.gameofthrones.data.storage.models.TitleDao;
import com.softdesign.gameofthrones.utils.CharacterEvent;
import com.softdesign.gameofthrones.utils.ConstantManager;
import com.softdesign.gameofthrones.utils.HouseEvent;
import com.softdesign.gameofthrones.utils.MessageEvent;
import com.softdesign.gameofthrones.utils.NetworkStatusChecker;
import com.softdesign.gameofthrones.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;

import static com.softdesign.gameofthrones.utils.ConstantManager.HOUSE_IDS;
import static com.softdesign.gameofthrones.utils.ConstantManager.HOUSE_NAME_RES;
import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_ERROR;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_NETWORK_NOT_AVAILABLE;
import static com.softdesign.gameofthrones.utils.ConstantManager.MES_SERVER_ERROR;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadDataService extends IntentService {


    public static boolean sIsRunning;

    private DataManager mDataManager = DataManager.getInstance();

    private HouseDao mHouseDao = mDataManager.getDaoSession().getHouseDao();
    private CharacterDao mCharacterDao = mDataManager.getDaoSession().getCharacterDao();
    private TitleDao mTitleDao = mDataManager.getDaoSession().getTitleDao();
    private AliasDao mAliasDao = mDataManager.getDaoSession().getAliasDao();


    public DownloadDataService() {
        super("DownloadDataService");
    }


    //идем за списком всех characters
    public static void startDownload(Context context) {
        Intent intent = new Intent(context, DownloadDataService.class);
        Log.d(LOG_TAG, "startDownload");
        context.startService(intent);
    }

    //нажали на родителя
    //если сервис запущен, в очередь не становимся - способ избежать обработки повторного нажатия
    public static boolean startDownloadParentCharacter(Context context, Long childChatacterId,Long characterId ) {
        if (!sIsRunning) {
            Intent intent = new Intent(context, DownloadDataService.class);
            intent.putExtra(ConstantManager.KEY_CHARACTER_ID, characterId);
            intent.putExtra(ConstantManager.KEY_CHILD_CHARACTER_ID, childChatacterId);
            Log.d(LOG_TAG, "startDownloadParentCharacter characterId=" + characterId + " childChatacterId=" + childChatacterId);
            context.startService(intent);
            return true;
        } else {
            Log.d(LOG_TAG, "NOT STARTED!! startDownloadParentCharacter characterId=" + characterId + " childChatacterId=" + childChatacterId);
            return false;

        }
    }

    //из списка нажали на characters
    public static boolean startDownloadCharacter(Context context, Long characterId, int houseIndex) {
        if (!sIsRunning) {
            Intent intent = new Intent(context, DownloadDataService.class);
            intent.putExtra(ConstantManager.KEY_CHARACTER_ID, characterId);
            intent.putExtra(ConstantManager.KEY_HOUSE_INDEX, houseIndex);
            Log.d(LOG_TAG, "startDownloadCharacter houseIndex=" + houseIndex);
            context.startService(intent);
            return true;
        } else {
            Log.d(LOG_TAG, "NOT STARTED!! startDownloadParentCharacter characterId=" + characterId + " houseIndex=" + houseIndex);
            return false;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sIsRunning = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sIsRunning = false;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        long characterId = intent.getLongExtra(ConstantManager.KEY_CHARACTER_ID, -1);
        long childCharacterId = intent.getLongExtra(ConstantManager.KEY_CHILD_CHARACTER_ID, -1);
        int houseIndex = intent.getIntExtra(ConstantManager.KEY_HOUSE_INDEX, -1);

        Log.d(LOG_TAG, "onHandleIntent characterId=" + characterId + " houseIndex=" + houseIndex);

        if (characterId == -1) {
            processCharacterList();
        } else {
            processCharacter(characterId, childCharacterId, houseIndex);
        }

        Log.d(LOG_TAG, "service finished");
    }


    private void processCharacter(Long characterId, Long childCharacterId, int houseIndex) {

        if (checkNetwork()) {
            CharacterRes characterRes = loadCharacter(characterId);

            if (characterRes != null) {
                CharacterDTO characterDTO = CharacterDTO.getInstance(characterRes);
                if (characterDTO.getFatherRemoteId() != null) {
                    long id = characterDTO.getFatherRemoteId();
                    CharacterRes characterFatherRes = loadCharacter(id);
                    if (characterFatherRes != null) {
                        characterDTO.setFatherName(characterFatherRes.getName());
                    }
                }
                if (characterDTO.getMotherRemoteId() != null) {
                    long id = characterDTO.getMotherRemoteId();
                    CharacterRes characterMotherRes = loadCharacter(id);
                    if (characterMotherRes != null) {
                        characterDTO.setMotherName(characterMotherRes.getName());
                    }
                }
                if (childCharacterId != -1) {
                    EventBus.getDefault().post(new CharacterEvent(characterDTO, childCharacterId));
                } else {
                    EventBus.getDefault().post(new CharacterEvent(characterDTO, houseIndex));
                }


            }
        }

    }


    private CharacterRes loadCharacter(Long characterId) {
        Call<CharacterRes> callCharacter = mDataManager.getCharacterById(characterId);
        try {
            Response<CharacterRes> response = callCharacter.execute();

            if (response.code() == 200) {
                CharacterRes characterRes = response.body();
                return characterRes;
            } else {
                EventBus.getDefault().post(new MessageEvent(MES_SERVER_ERROR));
            }


        } catch (Exception e) {
            EventBus.getDefault().post(new MessageEvent(MES_NETWORK_ERROR));
        }

        return null;
    }


    private void processCharacterList() {
        List<House> houseList = mHouseDao.queryRaw("WHERE remote_id IN (?,?,?)", HOUSE_IDS[0] + "", HOUSE_IDS[1] + "", HOUSE_IDS[2] + "");

        if (checkData(houseList)) {
            Log.d(LOG_TAG, "service: data is ready");

            sendData(houseList);

        } else if (checkNetwork()) {
            houseList = new ArrayList<>();
            House house = null;

            for (int i = 0; i < 3; i++) {
                house = createHouse(i);
                if (house != null) {
                    houseList.add(house);
                }
            }

            int page = 1;
            while (!checkData(houseList) && requestCharacterPage(houseList, page++)) ;

            sendData(houseList);
            saveData(houseList);

        }
    }

    private boolean checkData(List<House> houseList) {
        return houseList.size() == 3
                && houseList.get(0).isAllCharacters()
                && houseList.get(1).isAllCharacters()
                && houseList.get(2).isAllCharacters();
    }

    private void sendData(List<House> houseList) {
        //Log.d(LOG_TAG, "sendData started");
        for (int i = 0; i < 3; i++) {
            House house = houseList.get(i);
            List<Character> characterList = house.getCharacterList();
            List<CharacterDTO> characterDTOList = new ArrayList<>();
            for (Character character : characterList) {
                characterDTOList.add(CharacterDTO.getInstance(character));
            }
            EventBus.getDefault().post(new HouseEvent(characterDTOList, Utils.getHouseIndexById(house.getRemoteId())));


        }
        //Log.d(LOG_TAG, "sendData finished");
    }


    private void saveData(List<House> houseList) {
        Log.d(LOG_TAG, "saveData started");
        mHouseDao.insertOrReplaceInTx(houseList);

        for (House house : houseList) {

            List<Character> characterList = house.getCharacterList();
            mCharacterDao.insertOrReplaceInTx(characterList);

            for (Character character : house.getCharacterList()) {
                List<Title> titleList = character.getTitleList();
                if (titleList != null) {
                    mTitleDao.insertOrReplaceInTx(titleList);
                }
                List<Alias> aliasList = character.getAliasList();
                if (aliasList != null) {
                    mAliasDao.insertOrReplaceInTx(aliasList);
                }


            }

        }
        //Log.d(LOG_TAG, "saveData finished");


    }


    private boolean requestCharacterPage(List<House> houseList, int page) {

        Call<List<CharacterRes>> callCharacters = mDataManager.getCharacterList(page);
        try {
            Response<List<CharacterRes>> resp = callCharacters.execute();

            if (resp.code() == 200) {

                List<CharacterRes> characterResList = resp.body();
                for (CharacterRes characterRes : characterResList) {

                    List<String> allegiances = characterRes.getAllegiances();
                    for (String str : allegiances) {
                        long id = Utils.getIdFromUrl(str);
                        for (House house : houseList) {
                            if (id == house.getRemoteId()) {


                                Character character = new Character(house, characterRes);
                                house.addCharacter(character);

                                for (String title : characterRes.getTitles()) {
                                    character.addTitle(new Title(title, character.getRemoteId()));
                                }

                                for (String alias : characterRes.getAliases()) {
                                    character.addAlias(new Alias(alias, character.getRemoteId()));
                                }

                            }
                        }
                    }
                }
                Headers headers = resp.headers();
                String link = headers.get("Link");
                int pos1 = link.lastIndexOf("page=");
                int pos2 = link.lastIndexOf("&");
                int maxPage = Integer.parseInt(link.substring(pos1 + 5, pos2));//regexp надежнее, но так быстрее работает

                Log.d(LOG_TAG, "requestCharacterPage finished page=" + page + " maxPage=" + maxPage);
                return maxPage > page;


            } else {
                Log.e(LOG_TAG, "resp code=" + resp.code());
                EventBus.getDefault().post(new MessageEvent(MES_SERVER_ERROR));
            }
        } catch (Exception e) {
            EventBus.getDefault().post(new MessageEvent(MES_NETWORK_ERROR));
            Log.e(LOG_TAG, "", e);
        }

        return false;
    }


    /**
     * create house and empty characters
     *
     * @param index (in ui tabs)
     * @return
     */
    public House createHouse(int index) {

        try {

            long houseId = HOUSE_IDS[index];
            String houseName = getString(HOUSE_NAME_RES[index]);

            Call<HouseRes> callHouse = mDataManager.getHouseById(houseId);
            Response<HouseRes> response = callHouse.execute();

            if (response.code() == 200) {

                HouseRes houseRes = response.body();
                House house = new House(houseRes, houseName);

                return house;
/*
            } else if (response.code() == 304) {
                EventBus.getDefault().post(new MessageEvent(MES_NOT_MODIFIED));
*/

            } else {
                Log.e(LOG_TAG, "Network error: resp code=" + response.code() + " " + response.message());
                EventBus.getDefault().post(new MessageEvent(MES_NETWORK_ERROR));

            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "Network failure: ", e);
            EventBus.getDefault().post(new MessageEvent(MES_NETWORK_ERROR));
        }
        return null;
    }


    private boolean checkNetwork() {
        if (!NetworkStatusChecker.isNetworkAvailable(this)) {
            EventBus.getDefault().post(new MessageEvent(MES_NETWORK_NOT_AVAILABLE));
            return false;
        } else {
            return true;
        }
    }


}
