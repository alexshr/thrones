package com.softdesign.gameofthrones.utils;

import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;

import java.util.List;

public class HouseEvent {


    public Integer houseIndex;

    public List<CharacterDTO> characterList;


    public HouseEvent(List<CharacterDTO> characterList, Integer houseIndex) {

        this.characterList = characterList;
        this.houseIndex = houseIndex;
    }

    @Override
    public String toString() {
        return "HouseEvent{" +
                "houseIndex=" + houseIndex +
                ", characterList=" + characterList +
                '}';
    }
}
