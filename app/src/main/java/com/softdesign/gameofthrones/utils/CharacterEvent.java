package com.softdesign.gameofthrones.utils;

import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;

public class CharacterEvent {

    public int houseIndex;
    public Long childCharacterId;

    public CharacterDTO character;


    public CharacterEvent(CharacterDTO character, Long childCharacterId) {
        this.character = character;
        this.childCharacterId = childCharacterId;
    }

    public CharacterEvent(CharacterDTO character, int houseIndex) {
        this.character = character;
        this.houseIndex = houseIndex;
    }

    @Override
    public String toString() {
        return "CharacterEvent{" +
                "houseIndex=" + houseIndex +
                ", childCharacterId=" + childCharacterId +
                ", character=" + character +
                '}';
    }
}
