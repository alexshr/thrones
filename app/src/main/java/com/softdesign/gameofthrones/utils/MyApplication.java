package com.softdesign.gameofthrones.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.softdesign.gameofthrones.data.storage.models.DaoMaster;
import com.softdesign.gameofthrones.data.storage.models.DaoSession;

import org.greenrobot.greendao.database.Database;

public class MyApplication extends Application {

    public static SharedPreferences sSharedPreferences;
    public static Database db;
    private static Context sContext;
    private static DaoSession sDaoSession;

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


        db = new DaoMaster.DevOpenHelper(this, "gameofthrones.db").getWritableDb();

        sDaoSession = new DaoMaster(db).newSession();

        //для отладки
        //Stetho.initializeWithDefaults(this);
    }


}
