package com.softdesign.gameofthrones.utils;

import com.softdesign.gameofthrones.R;

public interface ConstantManager {
    String LOG_TAG = "DEV";

    //int PER_PAGE = 10; //for recyclerview pagging

    long HOUSE_STARK_ID = 362;
    long HOUSE_LANNISTER_ID = 229;
    long HOUSE_TARGARYEN_ID = 378;

    long[] HOUSE_IDS = new long[]{HOUSE_STARK_ID, HOUSE_LANNISTER_ID, HOUSE_TARGARYEN_ID};
    int[] HOUSE_NAME_RES = new int[]{R.string.house_starks, R.string.house_lannisters, R.string.house_targaryens};
    int[] HOUSE_WORDS_RES = new int[]{R.string.words_starks, R.string.words_lannisters, R.string.words_targaryens};
    int[] HOUSE_ICON_RES = new int[]{R.drawable.stark_icon, R.drawable.lanister_icon, R.drawable.targarien_icon};
    int[] HOUSE_LOGO_RES = new int[]{R.drawable.logo_stark, R.drawable.logo_lannister, R.drawable.logo_targarien};


    //String MES_NOT_MODIFIED = "MES_NOT_MODIFIED";
    String MES_NETWORK_ERROR = "MES_NETWORK_ERROR";
    String MES_SERVER_ERROR = "MES_SERVER_ERROR";
    String MES_NETWORK_NOT_AVAILABLE = "MES_NETWORK_NOT_AVAILABLE";


    //int MIN_TO_SHOW =10 ;//показыватьс список при мин. кол-ве элементов

    String MES_SHOW_PROGRESS = "MES_SHOW_PROGRESS";


    String KEY_CHARACTER = "KEY_CHARACTER";

    String KEY_CHARACTER_ID = "KEY_CHARACTER_ID";
    String KEY_HOUSE_INDEX = "KEY_HOUSE_INDEX";
    String KEY_CHILD_CHARACTER_ID = "KEY_CHILD_CHARACTER_ID";
}
