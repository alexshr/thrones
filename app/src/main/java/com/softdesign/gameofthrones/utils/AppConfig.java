package com.softdesign.gameofthrones.utils;

/**
 * Конфигурация приложения
 */
public interface AppConfig {
    String BASE_URL = "http://www.anapioficeandfire.com/api/";
    long MAX_CONNECT_TIMEOUT = 6000;
    long MAX_READ_TIMEOUT = 6000;

}
