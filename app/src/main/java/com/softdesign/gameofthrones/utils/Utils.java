package com.softdesign.gameofthrones.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.List;

import static com.softdesign.gameofthrones.utils.ConstantManager.HOUSE_IDS;
import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;


public class Utils {


    public static String convertList(List<String> list, String delimiter) {

        if (list.size() > 0) {
            StringBuilder sb = new StringBuilder("");
            for (String name : list) {
                sb.append(delimiter).append(name);
            }
            sb.delete(0, delimiter.length());
            return sb + "";
        }
        return null;
    }


    public static Long getIdFromUrl(String str) {
        if (str != null) {
            int pos = str.lastIndexOf("/");
            if (pos > -1) {
                str = str.substring(pos + 1);
                try {
                    Long res = Long.parseLong(str);
                    return res;
                } catch (NumberFormatException e) {
                    Log.e(LOG_TAG, "", e);
                }
            }
        }
        return null;
    }

    public static int getHouseIndexById(long id) {
        for (int i = 0; i < HOUSE_IDS.length; i++) {
            if (id == HOUSE_IDS[i]) {
                return i;
            }
        }
        return -1;
    }


    //http://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        return size.x;
    }


    /**
     * snackbar with  mes
     *
     * @param layout
     * @param mes
     */

    public static void showMessageOnSnackBar(CoordinatorLayout layout, String mes, boolean isError) {
        Snackbar snackbar = Snackbar.make(layout, mes, Snackbar.LENGTH_LONG);

        Snackbar.SnackbarLayout sLayout = (Snackbar.SnackbarLayout) snackbar.getView();

        TextView txtv = (TextView) sLayout.findViewById(android.support.design.R.id.snackbar_text);

        if (isError) {
            sLayout.setBackgroundColor(Color.RED);
            txtv.setTextColor(Color.WHITE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            txtv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }

        snackbar.show();
    }

    public static void showErrorOnSnackBar(CoordinatorLayout layout, String mes) {
        showMessageOnSnackBar(layout, mes, true);
    }

    public static void showInfoOnSnackBar(CoordinatorLayout layout, String mes) {
        showMessageOnSnackBar(layout, mes, false);
    }

    /*
    public static boolean isMyServiceRunning(Activity activity,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    */
}
