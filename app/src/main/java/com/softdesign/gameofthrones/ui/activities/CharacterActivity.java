package com.softdesign.gameofthrones.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.mvp.presenters.CharacterPresenter;
import com.softdesign.gameofthrones.mvp.views.ICharacterView;
import com.softdesign.gameofthrones.utils.ConstantManager;
import com.softdesign.gameofthrones.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.softdesign.gameofthrones.utils.ConstantManager.HOUSE_WORDS_RES;
import static com.softdesign.gameofthrones.utils.ConstantManager.KEY_CHARACTER;
import static com.softdesign.gameofthrones.utils.ConstantManager.KEY_HOUSE_INDEX;
import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;

/**
 * Сначала получаю данные в предыдущей активити и только потом перехожу на эту активити
 * Решил сделать так, чтоб не получилось, что с данными облом, а мы уже тут с белым экраном
 */

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.house_logo)
    ImageView mHouseLogo;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout mAppBar;
    @BindView(R.id.word)
    TextView mWord;
    @BindView(R.id.word_row)
    TableRow mWordRow;
    @BindView(R.id.born)
    TextView mBorn;
    @BindView(R.id.born_row)
    TableRow mBornRow;
    @BindView(R.id.died)
    TextView mDied;
    @BindView(R.id.died_row)
    TableRow mDiedRow;
    @BindView(R.id.title)
    TextView mTitles;
    @BindView(R.id.title_row)
    TableRow mTitleRow;
    @BindView(R.id.aliases)
    TextView mAliases;
    @BindView(R.id.aliases_row)
    TableRow mAliasesRow;

    @BindView(R.id.button_father)
    Button mButtonFather;
    @BindView(R.id.father_row)
    TableRow mFatherRow;
    @BindView(R.id.button_mother)
    Button mButtonMother;
    @BindView(R.id.mother_row)
    TableRow mMotherRow;
    private MenuItem mProgressMenuItem;

    private CharacterPresenter mPresenter;

    public static void start(@NonNull Context context, int houseIndex, @NonNull CharacterDTO character) {
        Intent intent = new Intent(context, CharacterActivity.class);
        intent.putExtra(KEY_HOUSE_INDEX, houseIndex);
        intent.putExtra(KEY_CHARACTER, character);
        context.startActivity(intent);
        Log.d(LOG_TAG, "CharacterActivity started characterId=" + character.getRemoteId() + " houseIndex=" + houseIndex);
    }

    /**
     * @param button
     */
    @OnClick(R.id.button_father)
    public void onFatherClick(Button button) {
        mPresenter.requestFather(this);
    }

    @OnClick(R.id.button_mother)
    public void onMatherClick(Button button) {
        mPresenter.requestMother(this);
    }


    @Override
    public void showCharacter(CharacterDTO character, int houseIndex) {
        Picasso.with(this)
                .load(ConstantManager.HOUSE_LOGO_RES[houseIndex])
                //TODO             /*.fit()  not works*/
                .into(mHouseLogo);

        mCollapsingToolbar.setTitle(character.getCharacterName());

        mWord.setText(HOUSE_WORDS_RES[houseIndex]);

        if (character.getBorn() != null && !character.getBorn().isEmpty()) {
            mBorn.setText(character.getBorn());
            mBornRow.setVisibility(View.VISIBLE);
        } else {
            mBornRow.setVisibility(View.GONE);
        }

        if (character.getDied() != null && !character.getDied().isEmpty()) {
            mDied.setText(character.getDied());
            mDiedRow.setVisibility(View.VISIBLE);
            Utils.showErrorOnSnackBar(mCoordinatorLayout, getString(R.string.character_died));
        } else {
            mDiedRow.setVisibility(View.GONE);
        }

        if (character.getFullTitle() != null) {
            mTitles.setText(character.getFullTitle());
            mTitleRow.setVisibility(View.VISIBLE);
        } else {
            mTitleRow.setVisibility(View.GONE);
        }

        if (character.getFullAlias() != null) {
            mAliases.setText(character.getFullAlias());
            mAliasesRow.setVisibility(View.VISIBLE);
        } else {
            mAliasesRow.setVisibility(View.GONE);
        }

        if (character.getFatherName() != null && character.getFatherRemoteId() != null) {
            mButtonFather.setText(character.getFatherName());
            mFatherRow.setVisibility(View.VISIBLE);
        } else {
            mFatherRow.setVisibility(View.GONE);
        }

        if (character.getMotherName() != null && character.getMotherRemoteId() != null) {
            mButtonMother.setText(character.getMotherName());
            mMotherRow.setVisibility(View.VISIBLE);
        } else {
            mMotherRow.setVisibility(View.GONE);
        }
    }

    @Override
    public void startCharacterActivity(CharacterDTO character, int houseIndex) {
        CharacterActivity.start(this,houseIndex,character);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        ButterKnife.bind(this);
        initToolbar();

        mPresenter = new CharacterPresenter(this);

        CharacterDTO character=null;
        int houseIndex=0;
        if (savedInstanceState != null) {
            houseIndex = savedInstanceState.getInt(KEY_HOUSE_INDEX);
            character = savedInstanceState.getParcelable(KEY_CHARACTER);
        } else {
            character = getIntent().getParcelableExtra(KEY_CHARACTER);
            houseIndex = getIntent().getIntExtra(KEY_HOUSE_INDEX, -1);
        }
        mPresenter.init(character,houseIndex);


    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Title");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_CHARACTER, mPresenter.getCharacter());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        mPresenter.register();


    }



    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.unregister();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.throne_list, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        mProgressMenuItem = menu.findItem(R.id.miActionProgress);


        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void showProgress() {
        mProgressMenuItem.setVisible(true);
    }

    @Override
    public void hideProgress() {
        mProgressMenuItem.setVisible(false);
    }



    @Override
    public void showError(int mes) {
        Utils.showErrorOnSnackBar(mCoordinatorLayout, getString(mes));
    }
}
