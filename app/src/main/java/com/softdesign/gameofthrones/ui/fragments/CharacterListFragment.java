package com.softdesign.gameofthrones.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.mvp.presenters.CharacterListPresenter;
import com.softdesign.gameofthrones.mvp.views.ICharacterListView;
import com.softdesign.gameofthrones.ui.activities.CharacterActivity;
import com.softdesign.gameofthrones.ui.adapters.CharacterListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.softdesign.gameofthrones.utils.ConstantManager.KEY_HOUSE_INDEX;
import static com.softdesign.gameofthrones.utils.ConstantManager.LOG_TAG;


public class CharacterListFragment extends Fragment implements ICharacterListView {

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private List<CharacterDTO> mCharacterList = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;
    private CharacterListAdapter mCharacterListAdapter;

    private CharacterListPresenter mPresenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        int houseIndex = getArguments().getInt(KEY_HOUSE_INDEX);

        mPresenter=new CharacterListPresenter(this,houseIndex);


        Log.d(LOG_TAG, "onCreate houseIndex=" + houseIndex);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_house_tab, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mCharacterListAdapter = new CharacterListAdapter(mCharacterList, mPresenter.getHouseIndex(), new CharacterListAdapter.UserViewHolder.CharacterItemClickListener() {
            @Override
            public void onUserItemClick(int adapterPosition, View view) {
                CharacterDTO character = mCharacterListAdapter.getCharacter(adapterPosition);
                mPresenter.requestCharacter(getActivity(),character.getRemoteId());

            }
        });

        mRecyclerView.setAdapter(mCharacterListAdapter);


    }

    @Override
    public void onStart() {

        super.onStart();

        mPresenter.register();
        if (mCharacterList != null && !mCharacterList.isEmpty()) {
            mPresenter.showCharacterList(mCharacterList);
        }

        Log.d(LOG_TAG, "fragment onStart  registered!!");


    }

    public void showCharacterList(List<CharacterDTO> characterList){
        mCharacterListAdapter.swap(characterList);
    }

    @Override
    public void startCharacterActivity(CharacterDTO character, int houseIndex) {
        CharacterActivity.start(getActivity(), houseIndex, character);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unregister();
    }


}