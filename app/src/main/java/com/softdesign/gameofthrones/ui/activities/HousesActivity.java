package com.softdesign.gameofthrones.ui.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.mvp.presenters.HousesPresenter;
import com.softdesign.gameofthrones.mvp.views.IHousesView;
import com.softdesign.gameofthrones.ui.fragments.CharacterListFragment;
import com.softdesign.gameofthrones.utils.ConstantManager;
import com.softdesign.gameofthrones.utils.Utils;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.softdesign.gameofthrones.utils.ConstantManager.KEY_HOUSE_INDEX;

public class HousesActivity extends AppCompatActivity implements IHousesView, NavigationView.OnNavigationItemSelectedListener {

    //стартует ли впервые - для mSplash
    private static boolean sIsFirstStart = true;



    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.viewpager)
    ViewPager mViewPager;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;

    @BindView(R.id.splash)
    ImageView mSplash;
    private MenuItem mProgressMenuItem;

    private HousesPresenter mPresenter = HousesPresenter.getInstance();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_throne_list);
        ButterKnife.bind(this);

        mPresenter.takeView(this);

        init();

    }


    private void init() {


        setSupportActionBar(mToolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        mTabLayout.setupWithViewPager(mViewPager);//


        HousePagerAdapter pagerAdapter = new HousePagerAdapter(getSupportFragmentManager());

        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(pagerAdapter);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Store instance of the menu item containing progress
        mProgressMenuItem = menu.findItem(R.id.miActionProgress);
        // Extract the action-view from the menu item
        ProgressBar v = (ProgressBar) MenuItemCompat.getActionView(mProgressMenuItem);
        // Return to finish
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.register();

        if (sIsFirstStart) {
            showSplash();
            sIsFirstStart = false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //DownloadDataService.startDownload(HousesActivity.this);
                mPresenter.requestData(HousesActivity.this);
            }
        }, 50);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unregister();
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.throne_list, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        mDrawerLayout.closeDrawer(GravityCompat.START);

        int id = item.getItemId();
        switch (id) {
            case R.id.starks:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.lannisters:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.targaryens:
                mViewPager.setCurrentItem(2);

        }

        return true;
    }


    public void showSplash() {
        Picasso.with(this).load(R.drawable.splash).fit().into(mSplash);
        mSplash.setVisibility(View.VISIBLE);
        //mIsSlashVisible = true;
    }


    public void hideSplash() {
        mSplash.setVisibility(View.GONE);
        //mIsSlashVisible = false;
    }

    @Override
    public void showError(int mes) {
        Utils.showErrorOnSnackBar(mCoordinatorLayout,getString(mes));
    }


    public void showProgress() {
        // Show progress item
        mProgressMenuItem.setVisible(true);
    }

    public void hideProgress() {
        // Hide progress item
        mProgressMenuItem.setVisible(false);
    }

    private class HousePagerAdapter extends FragmentStatePagerAdapter {


        public HousePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            CharacterListFragment fragment = new CharacterListFragment();

            Bundle args = new Bundle();
            args.putInt(KEY_HOUSE_INDEX, i);
            //args.putInt(CharacterListFragment.KEY_HOUSE_NAME_RES, ConstantManager.HOUSE_NAME_RES[i]);
            fragment.setArguments(args);
            return fragment;
        }

/*
        public Fragment findFragmentByPosition(int position) {

            return getSupportFragmentManager().findFragmentByTag(
                    "android:switcher:" + mViewPager.getId() + ":"
                            + getItemId(position));
        }
*/

        @Override
        public int getCount() {
            return 3;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            return getString(ConstantManager.HOUSE_NAME_RES[position]);
        }

    }

}
