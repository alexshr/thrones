package com.softdesign.gameofthrones.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.gameofthrones.R;
import com.softdesign.gameofthrones.data.storage.models.CharacterDTO;
import com.softdesign.gameofthrones.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * adapter for user info list
 */
public class CharacterListAdapter extends RecyclerView.Adapter<CharacterListAdapter.UserViewHolder> {

    private Context mContext;
    private int mHouseIndex;

    private List<CharacterDTO> mCharacterList;
    private UserViewHolder.CharacterItemClickListener mCharacterItemClickListener;


    public CharacterListAdapter(List<CharacterDTO> characterList, int houseIndex, UserViewHolder.CharacterItemClickListener characterItemClickListener) {
        mCharacterList = characterList;
        mCharacterItemClickListener = characterItemClickListener;
        mHouseIndex = houseIndex;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.character_list_item, parent, false);

        return new UserViewHolder(convertView, mCharacterItemClickListener);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, int position) {


        CharacterDTO character = mCharacterList.get(position);

        Picasso.with(mContext)
                .load(ConstantManager.HOUSE_ICON_RES[mHouseIndex])
                .fit()
                .into(holder.iconView);

        holder.nameView.setText(character.getCharacterName());
        if (character.getInfoForList() != null) {
            holder.infoView.setText(character.getInfoForList());
        }

    }

    @Override
    public int getItemCount() {
        return mCharacterList.size();
    }


    public CharacterDTO getCharacter(int position) {
        return mCharacterList.get(position);
    }

    public void swap(List<CharacterDTO> data) {
        mCharacterList.clear();
        mCharacterList.addAll(data);
        notifyDataSetChanged();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.icon)
        ImageView iconView;

        @BindView(R.id.name)
        TextView nameView;

        @BindView(R.id.info)
        TextView infoView;

        private CharacterItemClickListener mListener;

        public UserViewHolder(View itemView, CharacterItemClickListener characterItemClickListener) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            this.mListener = characterItemClickListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onUserItemClick(getAdapterPosition(), v);
                }
            });

        }


        public interface CharacterItemClickListener {
            void onUserItemClick(int adapterPosition, View view);
        }
    }


}
